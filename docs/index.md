---
Title: 2025 Big Ride
Date: 2025-05-15
---

# The Plan

To ride bikes across the USA, from West to East

![Overview map](images/rwgps_collection_overview_map.png)

* [Tracking Map (SPOT Tracking)]
    * This will go live on Saturday, May 17, 2025
    * Ask either of us for the password
* [RwGPS Route Collection]
* [Private info ssht][ssht]
    * Ask either of us for access permissions


# The trip


=== "Attractions"

    * Meeting up and riding with family and friends along the way!
    * [EAA Aviation Museum]
    * [Hamilton Wood Type & Printing Museum]
    * [A very LARGE pigeon]

=== "The beginning"

    * SMART train from Santa Rosa to Larkspur
    * Ride from Larkspur to Richmond
    * Amtrak from Richmond to Martinez to Eugene, OR

=== "The middle"

    ...lots and lots of circles :recycle:...

=== "The end"

    Finish in Brooklyn


[Tracking Map (SPOT Tracking)]: https://maps.findmespot.com/s/Z1NQ
[RwGPS Route Collection]: https://ridewithgps.com/collections/2721340
[ssht]: https://docs.google.com/spreadsheets/d/1-ZuZcK0hhBpJ30zeSGabXTMycgXSSrWfxGEN61T1dng/edit?gid=1374835991#gid=1374835991
[amtrak]: https://www.amtrak.com/guestrewards/account-overview/my-trips.html
[Hamilton Wood Type & Printing Museum]: https://woodtype.org/ "woodtype.org"
[EAA Aviation Museum]: https://www.eaa.org/eaa-museum
[A very LARGE pigeon]: https://annekadet.substack.com/p/pigeon?open=false#%C2%A7new-york-is-in-love-with-a-foot-pigeon

