---
title: Prepration for the ride
description: A run-down of what we are doing to prepare for the cross-country ride.
date: 2025-01-01
author: Eric
---

# Preparation for the ride

So many yaks to shave!

<!-- more -->

We decided to ride our bikes across the country early in 2024. Well, I decided I wanted to do the
ride and I invited Megan to join me, which she seemed excited about. Right, make a list of
requirements:

- [x] Touring bikes

Our [Fitz] rando bikes might not work for this trip. They were designed for Randonneuring and are
excellent for that style of riding. But, we've tried taking them on a couple of small tours with
front panniers and found that they would [oscillate] when fully loaded.

- [x] Time off work

My boss was encouraging, saying _How can I refuse when you are giving me a year to plan for it?_
So, we agreed on a leave-of-absense due to the long duration of the trip.


[Fitz]: http://www.fitzcyclez.com/
[oscillate]: https://photos.google.com/share/AF1QipM7ntRNQp16GiZnZ6iu955f0oOmGrrRg5p5h_EOrOxUAPK-Tpyxl5W-7QBYXPqCyg/photo/AF1QipOBnWFr75Vviba7xRgfcgvDY8oU5KBjNxk1qEnl?key=djdEU0paTDZpTWlKc3hJQ21ZWUxBM0p3RERCLVVn

