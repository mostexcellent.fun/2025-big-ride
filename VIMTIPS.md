# Vim Tips

## NERDTree

It adds a file tree to the left side of vim.

* Toggle NERDTree window: `,n`
* Create a new file:
    * move the cursor to the directory in which to create the new file
    * `m` to open the NERDTree menu
    * `a`dd the new file
* Use Ctrl-w < and Ctrl-w > to manually resize NERDTree.

## Navigating open files (buffers)

* See the list of files: `:ls`
* Switch to a file from the list: `:b<n>` where `n` is the file number from `:ls`

## Project sessions

There's a session file in this directory: `session.big-ride.vim`.

* Reload the session with: `:source session.big-ride.vim`
* To update or create a session file: `:mksession! <session file name>`
    * `:mksession! session.big-ride.vim`

