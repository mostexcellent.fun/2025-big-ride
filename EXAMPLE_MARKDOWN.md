# mkdocs-material markdown examples

## Code block with title, line numbers, and highlighted lines

```python title="greeting.py" linenums="1" hl_lines="1-2"

def foo(bar="fizz"):
    print(f"Hello {bar}")

```


## Content tabs

=== "The beginning"

    * SMART train from Santa Rosa to Larkspur
    * Ride from Larkspur to Richmond
    * Amtrak from Richmond to Martinez to Eugene, OR

=== "The middle"

    ...lots and lots of circles :recycle:...

=== "The end"

    Finish in Brooklyn

## Admonitions

!!! note "This is the note title"

    The note contents here!

??? info "A collapsible callout"

    This content is collapsible

