# 2025 Big Ride repository

The main pages are in the `docs` directory, with [index.md](https://ewalstad.gitlab.io/2025-big-ride/)([edit](https://gitlab.com/-/ide/project/ewalstad/2025-big-ride/edit/main/-/docs/index.md)) being the home page.

GitLab has [an integrated development environment (IDE)](https://gitlab.com/-/ide/project/ewalstad/2025-big-ride/edit/main/-/)
where the pages can be edited in a web browser. To save your work, _commit_ your changes:

1. Click on the _Source Control_ icon in the left panel
1. Enter a brief commit message describing the changes you made.
1. Click the _Commit & Push_ button.
1. When prompted, choose _No  use the current branch "main"_.

## Working with mkdocs and its plugins

* https://www.mkdocs.org/user-guide/
* https://squidfunk.github.io/mkdocs-material/creating-your-site/
* https://github.com/guts/mkdocs-rss-plugin
* https://www.youtube.com/watch?v=xlABhbnNrfI
* https://squidfunk.github.io/mkdocs-material/reference/icons-emojis
